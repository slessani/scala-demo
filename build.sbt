name := "scala-demo"

idePackagePrefix := Some("demo.scala")

val standardSettings: Seq[Def.Setting[_]] =
  Seq[Def.Setting[_]](
    organization := "demo.scala",
    scalaVersion := "2.13.6",
    Test / scalacOptions ++= Seq("-Yrangepos"), // Specs2
    Compile / javacOptions := Seq("-encoding", "UTF-8"),
    version := "0.1.0",
    crossPaths := false,
    resolvers += "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/releases/",
    resolvers += Resolver.bintrayRepo("outworkers", "oss-releases"),
    resolvers += Classpaths.typesafeReleases,
    Test / parallelExecution := false,
    testOptions += Tests.Argument(TestFrameworks.JUnit, "-q", "-v", "-s", "-a"),
    logBuffered := false,
  )
/*
lazy val root = (project in file("."))
  .settings(standardSettings)
*/

lazy val `hello-world` = (project in file("hello-world"))
  .settings(standardSettings)

lazy val traits = (project in file("traits"))
  .settings(standardSettings)

lazy val `data-structures` = (project in file("data-structures"))
  .settings(standardSettings)

lazy val shapeless = (project in file("shapeless"))
  .settings(standardSettings)
  .settings(libraryDependencies ++= Seq("com.chuusai" %% "shapeless" % "2.3.3")
  )
lazy val async = (project in file("async"))
  .settings(standardSettings)



