package demo.scala.async

import java.util.concurrent.Executors
import scala.concurrent.duration.Duration
import scala.concurrent.blocking
import scala.concurrent.{Await, ExecutionContext, Future}


object AsyncApp extends App {

  //import scala.concurrent.ExecutionContext.Implicits.global

  implicit val executionContext: ExecutionContext =
    ExecutionContext.fromExecutor(Executors.newFixedThreadPool(32))

  def longComputation(i: Int): Future[Int] = Future {
    Thread.sleep(1000)
    i * 2
  }

  val range = 1 to 32

  def computeSeqOfInt(seq: Seq[Int]): Seq[Int] = {
    val fs = seq map longComputation
    val f = Future.sequence(fs)
    val res = Await.result(f, Duration.Inf)
    res
  }

  def computeSeqOfIntV2(seq: Seq[Int]): Seq[Int] = {
    val fs = for (i <- seq) yield {
      val f = longComputation(i)
      for (j <- f) yield j + 1
    }
    val f = Future.sequence(fs)
    val res = Await.result(f, Duration.Inf)
    res
  }

  def timeIt[A](body: => A): A = {
    val start = System.currentTimeMillis()
    val res = body
    val end =  System.currentTimeMillis()
    println(s"Elapsed time in (ms): ${end - start}")
    res
  }

  val computedRange = timeIt(computeSeqOfIntV2(range))
  println(computedRange)
  sys.exit(0)
}
