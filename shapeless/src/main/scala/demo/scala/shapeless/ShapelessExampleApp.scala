package demo.scala.shapeless

import shapeless._
import shapeless.syntax.singleton.mkSingletonOps
import shapeless.poly.identity

case class Person(name: String, age: Int)

case class Employee(name: String, age: Int, department: String)

object plusOne extends Poly1 {
  implicit def caseInt =
    at[Int]{ _ + 1 }

  implicit def caseString =
    at[String]{ _ + 1 }

  implicit def caseDouble =
    at[Double]{ _ + 1 }

  implicit def casePerson =
    at[Person] { p => p.copy(age = p.age + 1) }
}

object Conversion {

  val genPerson = LabelledGeneric[Person]
  val genEmployee = LabelledGeneric[Employee]

  def person2Employee(person: Person, department: String): Employee = {
    val l = genPerson.to(person)
    val extra = Symbol("department") ->> department :: HNil
    val el = (l :: extra :: HNil) flatMap identity
    genEmployee.from(el)
  }

  def employee2Person(employee: Employee): Person = {
    val l = genEmployee.to(employee).take(2)
    genPerson.from(l)
  }

}

object ShapelessExampleApp extends App {

  val hl = HList(1, "String", 2.45, Person("Bob", 20))

  val two = hl(0) * 2

  println(two)
  println(hl.select[Int])
  val mapped = hl.map(plusOne)
  println(mapped)

  val employee = Conversion.person2Employee(hl(3), "IT")
  println(employee)
  println(Conversion.employee2Person(employee))

}
