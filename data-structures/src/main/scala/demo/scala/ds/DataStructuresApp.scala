package demo.scala.ds

import scala.util.Try

object DataStructuresApp extends App {

  def parse(input: String): Seq[Seq[Int]] = {
    input.split(System.lineSeparator).toSeq.map(_.split(";").map(_.toInt).toSeq)
  }

  def safeParse(input: String): Seq[Seq[Option[Int]]] = {
    input.split(System.lineSeparator).toSeq.map(_.split(";").map(e => Try(e.toInt).toOption).toSeq)
  }

  def multipleLoop(n: Int): Seq[(Int, Int)] = {
    val res = for {
      i <- 1 to n
      j <- 1 to n if j < i && j%2 == 0
    } yield (i,j)
    res
  }

  val safeData =
    """1;2
      |3;4
      |5;6;7
      |""".stripMargin

  val unsafeData =
    """1;2
      |3;4;x;y
      |5;6;7
      |""".stripMargin

  val res1 = parse(safeData).flatten
  println(res1)

  val res2 = safeParse(unsafeData).flatten.flatten
  println(res2)

  val product = res2.foldLeft(1)(_ * _)

  val scan = res2.scanLeft(1)(_ * _)

  val partition = res2 partition(_ % 2 == 0)

  println(s"Evens/odds partition = $partition")

  println(s"Product = $product")

  println(s"Product scan = $scan")

  val ml = multipleLoop(10)

  println(ml)

  val reduced = ml map(t => t._1 + t._2)
  println(s"Reduced result: $reduced")


}
