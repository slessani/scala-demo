package demo.scala.traits

abstract class Printer[-A] {
  def print(value: A): Unit
}

class AnimalPrinter extends Printer[Animal] {
  def print(animal: Animal): Unit =
    println("The animal's age is: " + animal.age)
}

class CatPrinter extends Printer[Cat] {
  def print(cat: Cat): Unit =
    println("The cat's age is: " + cat.age)
}

object AnimalPrinterApp extends App {

  def printAnimalAges(animals: List[Animal]): Unit =
    animals foreach {
      animal => println(animal.age)
    }

  val cats: List[Cat] = List(Cat(1), Cat(2))
  val dogs: List[Dog] = List(Dog(3), Dog(4))

  def printMyCat(printer: Printer[Cat], cat: Cat): Unit = printer.print(cat)

  val catPrinter: Printer[Cat] = new CatPrinter
  val animalPrinter: Printer[Animal] = new AnimalPrinter

  printAnimalAges(cats)
  printAnimalAges(dogs)

  printMyCat(catPrinter, Cat(1))
  printMyCat(animalPrinter, Cat(1))

}
