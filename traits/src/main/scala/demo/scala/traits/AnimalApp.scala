package demo.scala.traits

object AnimalApp extends App {

  def identify(any: Any): Unit = {
    println("-" * 80)
    any match {
      case fswa: Animal with FlyingAnimal with WalkingAnimal with SwimmingAnimal =>
        fswa.fly()
        fswa.swim()
        fswa.walk()
      case swa: Animal with WalkingAnimal with SwimmingAnimal =>
        swa.swim()
        swa.walk()
      case sa: SwimmingAnimal => sa.swim()
      case fa: FlyingAnimal => fa.fly()
      case wa: WalkingAnimal => wa.walk()
      case da: Animal with Display => println(s"Found an animal of type: ${da.displayType} of age: ${da.age}")
      case a: Animal => println(s"Found an animal of age: ${a.age}")
      case pets @ List(Cat(_), Dog(age)) => println(s"Found pets: a cat and a $age year old dog")
        pets foreach identify
      case List(Cat(_), Dog(_), _*) => println(s"Found a cat dog and some other stuff")
      case 1 | 2 => println("Found one or two")
      case _ => println("Could not be processed...")
    }
  }

  val dog = Dog(4)
  val cat = Cat(5)
  val duck = Duck(2)
  val penguin = Penguin(3)
  val snake = Animal("Snake", 2)
  val unknownAnimal = Animal("Lion", 6)
  identify(dog)
  identify(penguin)
  identify(duck)
  identify(snake)
  identify(unknownAnimal)
  identify(List(cat,dog))
  identify(List(cat,dog, "Some other stuff..."))
  identify(2)
  identify("Foo")
}
