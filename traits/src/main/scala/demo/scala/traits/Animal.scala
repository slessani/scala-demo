package demo.scala.traits

sealed abstract class Animal {
  val age: Int
}

object Animal {
  def apply(value: String, age: Int): Animal = {
    value match {
      case "Duck" | "duck" => Duck(age)
      case "Dog" | "dog" => Dog(age)
      case "Cat" | "cat" => Cat(age)
      case "Snake" | "snake" => new Snake(age)
      case "Penguin" | "penguin" => new Snake(age)
      case  _ => new UnknownAnimal(value)
    }
  }
}

trait Display {
  def displayType: String = getClass.getSimpleName
}

trait FlyingAnimal extends Display {
  def fly(): Unit = println(s"flying $displayType")
}

trait WalkingAnimal extends Display {
  def walk(): Unit = println(s"walking $displayType")
}

trait ClumsyWalkingAnimal extends WalkingAnimal {
  override def walk(): Unit = println(s"Clumsy walking $displayType")
}

trait SwimmingAnimal extends Display {
  self: Animal =>
  def swim(): Unit = println(s"$age year old $displayType is swimming")
}

case class Duck(age: Int) extends Animal
  with FlyingAnimal
  with WalkingAnimal
  with SwimmingAnimal

case class Dog(age: Int) extends Animal with WalkingAnimal

case class Cat(age: Int) extends Animal with WalkingAnimal

case class Penguin(age: Int) extends Animal with SwimmingAnimal with ClumsyWalkingAnimal

class Snake(val age: Int = 1) extends Animal with Display

class UnknownAnimal(val species: String) extends Animal {
  val age = 0
}




